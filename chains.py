# -*- coding: utf-8 -*-
known_chains = {
    "GPH": {
        "chain_id": "7fcf452d6bb058949cdc875b13c8908c8f54b0f264c39faf8152b682af0740ee",
        "core_symbol": "GPH",
        "prefix": "GPH",
    },
    "CTS": {
        "chain_id": "53952fc10d52a3af6d572a298e8f99a0a2e7afd0f7dbad354d8d65e8c36c962d",
        "core_symbol": "CTS",
        "prefix": "CTS",
    },
    "TEST": {
        "chain_id": "39f5e2ede1f8bc1a3a54a7914414e3779e33193f1f5693510e73cb7a87617447",
        "core_symbol": "TEST",
        "prefix": "TEST",
    },
    "YMIR": {
        "chain_id": "7c1c72eb738b3ff1870350f85daca27e2d0f5dd25af27df7475fbd92815e421e",
        "core_symbol": "TEST",
        "prefix": "TEST",
    },
}
